package com.example.jorge.tareasignos;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    TextView tv;
    Calendar mCurrentDate;
    int day, month, year;
    int diasigno, messigno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv =(TextView) findViewById(R.id.textView);
        mCurrentDate = Calendar.getInstance();

        day=mCurrentDate.get(Calendar.DAY_OF_MONTH);
        month=mCurrentDate.get(Calendar.MONTH);
        year=mCurrentDate.get(Calendar.YEAR);

        month=month+1;

        tv.setText(day+"/"+month+"/"+year);


        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog=new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfyear, int dayOfMonth) {
                        monthOfyear = monthOfyear+1;
                        tv.setText(dayOfMonth+"/"+monthOfyear+"/"+year);
                            messigno = monthOfyear;
                            diasigno = dayOfMonth;
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });

        final Button buttonconfirmar=findViewById(R.id.confirmar);

        buttonconfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signos(diasigno,messigno);
            }
        });
    }

    public void obtenerFecha(int mes,int dia){
        diasigno = dia;
        messigno = mes;
    }

    public void signos(int dia, int mes) {
        switch (mes) {
            case 1:
                if (dia > 21) {
                    Intent intent=new Intent(MainActivity.this,AcuarioActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent=new Intent(MainActivity.this,CapricornioActivity.class);
                    startActivity(intent);
                }
                break;
            case 2:
                if (dia > 19) {
                    Intent intent=new Intent(MainActivity.this,PiscisActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent=new Intent(MainActivity.this,AcuarioActivity.class);
                    startActivity(intent);
                }
                break;
            case 3:
                if (dia > 20) {
                    Intent intent=new Intent(MainActivity.this,AriesActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent=new Intent(MainActivity.this,PiscisActivity.class);
                    startActivity(intent);
                }
                break;
            case 4:
                if (dia > 20) {
                    Intent intent=new Intent(MainActivity.this,TauroActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent=new Intent(MainActivity.this,AriesActivity.class);
                    startActivity(intent);
                }
                break;
            case 5:
                if (dia > 21) {
                    Intent intent=new Intent(MainActivity.this,GeminisActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent=new Intent(MainActivity.this,TauroActivity.class);
                    startActivity(intent);
                }
                break;
            case 6:
                if (dia > 20) {
                    Intent intent=new Intent(MainActivity.this,CancerActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent=new Intent(MainActivity.this,GeminisActivity.class);
                    startActivity(intent);
                }
                break;
            case 7:
                if (dia > 22) {
                    Intent intent=new Intent(MainActivity.this,LeoActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent=new Intent(MainActivity.this,CancerActivity.class);
                    startActivity(intent);
                }
                break;
            case 8:
                if (dia > 21) {
                    Intent intent=new Intent(MainActivity.this,VirgoActivity.class);
                    startActivityForResult(intent, 0);
                } else {
                    Intent intent=new Intent(MainActivity.this,LeoActivity.class);
                    startActivity(intent);
                }
                break;
            case 9:
                if (dia > 22) {
                    Intent intent=new Intent(MainActivity.this,LibraActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent=new Intent(MainActivity.this,VirgoActivity.class);
                    startActivityForResult(intent, 0);
                }
                break;
            case 10:
                if (dia > 22) {
                    Intent intent=new Intent(MainActivity.this,EscorpioActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent=new Intent(MainActivity.this,LibraActivity.class);
                    startActivity(intent);
                }
                break;
            case 11:
                if (dia > 21) {
                    Intent intent=new Intent(MainActivity.this,SagitarioActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent=new Intent(MainActivity.this,EscorpioActivity.class);
                    startActivity(intent);
                }
                break;
            case 12:
                if (dia > 21) {
                    Intent intent=new Intent(MainActivity.this,CapricornioActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent=new Intent(MainActivity.this,SagitarioActivity.class);
                    startActivity(intent);
                }
                break;
        }
    }
}
